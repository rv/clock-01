#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <TM1637Display.h>

#define COLON_ON 1
#define COLON_OFF 2
#define COLON_BLINK 3

const int brightness = 4;
const int colonType = COLON_BLINK;
const bool zeroHour = false;

const int CLK = D1;
const int DIO = D2;

#define COLON 0b01000000

TM1637Display display(CLK, DIO);

const char *ssid = "Zeeraket_Guest";
const char *password = "badgast1949";

#define TIME_ZONE (+2)

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "nl.pool.ntp.org");

void setupWifi(void);
void setupTimeClient(void);
void displayCode(int, unsigned long);
void setupDisplay(void);
uint8_t showColon(int);
void showTime(int, int, int);
void updateTime(void);

void setupWifi()
{
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
}

void setupTimeClient()
{
  timeClient.begin();
  timeClient.setTimeOffset(TIME_ZONE * (60 * 60));
  timeClient.setUpdateInterval(300 * 1000); // 5 minute interval
}

void displayCode(int code, unsigned long time) {
  display.showNumberDecEx(code, COLON);
  delay(time);
}

void setupDisplay() {
  display.clear();
  display.setBrightness(brightness);
  displayCode(0, 0);
}

uint8_t showColon(int seconds) {
  uint8_t colon;
  switch (colonType) {
    case COLON_ON:
      colon = COLON;
      break;
    case COLON_OFF:
      colon = 0;
      break;
    case COLON_BLINK:
      colon = seconds % 2 ? COLON : 0;
      break;
    default:
      colon = 0;
  }
  return colon;
}

void showTime(int hours, int minutes, int seconds)
{
  int value = hours * 100 + minutes;
  display.showNumberDecEx(value, showColon(seconds), zeroHour, 4, 0);
}

void updateTime()
{
  static char buf[10] = {'\0'};
  static char prev[10] = {'\0'};
  int cur_hour = 0;
  int cur_min = 0;
  int cur_sec = 0;

  timeClient.update();
  cur_hour = timeClient.getHours();
  cur_min = timeClient.getMinutes();
  cur_sec = timeClient.getSeconds();
  sprintf(buf, "%02d%02d%02d", cur_hour, cur_min, cur_sec);
  if (strcmp(buf, prev) != 0)
  {
    showTime(cur_hour, cur_min, cur_sec);
    strcpy(prev, buf);
  }
  delay(100);
}

void setup()
{
  Serial.begin(115200);
  delay(2000);
  Serial.println("Setup WiFi");
  setupWifi();
  Serial.println("Setup TimeClient");
  setupTimeClient();
  Serial.println("Setup Display");
  setupDisplay();
  Serial.println("Setup done");
}

void loop()
{
  updateTime();
}
