#pragma once

#include <Timezone.h>
#include "display.h"

void updateTime(void);
void showTime(void);
void printTime(time_t);
void test(void);
