#pragma once

#include <TimeLib.h>
#include <WiFiUdp.h>
#include <ESP8266WiFi.h>
#include "display.h"

void sendNTPpacket(IPAddress &);
time_t getNtpTime();
void setupNTP();