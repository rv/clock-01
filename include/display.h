#pragma once

#include <Arduino.h>
#include <TM1637Display.h>
#include <TimeLib.h>
#include <IPAddress.h>

#define COLON_ON 1
#define COLON_OFF 2
#define COLON_BLINK 3

void setupDisplay(void);
void displayCode(int, unsigned long = 500);
void displayTime(time_t);
uint8_t showColon(time_t);
void displayIp(IPAddress);