#pragma once

#define USE_LITTLEFS true
#define ESP_DRD_USE_LITTLEFS true
#include <ESP_DoubleResetDetector.h>
#define DRD_TIMEOUT 5
#define DRD_ADDRESS 0

extern DoubleResetDetector* drd;
extern bool doubleResetDetected;

bool setupDoubleResetDetect(void);
void loopDoubleResetDetect(void);