#pragma once

#include <LittleFS.h>
#include "display.h"

#define CONFIG_FILENAME "/config.json"

typedef struct {
  int brightness;
  int colonType;
  bool zeroHour;
} ConfigValues;

class Config {
  private:
    int brightness = 6;
    int colonType = COLON_BLINK;
    bool zeroHour = false;
  public:
    Config();
    void setup(void);
    void setBrightness(int);
    int getBrightness(void);
    void setColonType(int);
    int getColonType(void);
    void setZeroHour(bool);
    bool getZeroHour(void);
    void initConfigFilesystem(void);
    void initConfigValues(void);
    void loadConfigValues(void);
    void saveConfigValues(void);
};

extern Config config;
