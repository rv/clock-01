#pragma once

#include <ESPAsyncWebServer.h>
#include <DNSServer.h>

extern AsyncWebServer webServer;
extern DNSServer dnsServer;

void setupWebserver();
void webRootPage(AsyncWebServerRequest *);
void webNotFound(AsyncWebServerRequest *);
